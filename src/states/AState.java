package states;

import conv.Messages;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public abstract class AState {
    protected boolean hasNext;
    private final ESTATE nextState;

    private final Socket socket;
    private final BufferedWriter writer;
    private final BufferedReader reader;
    protected String currentMsg;

    public AState(Socket socket,
                  BufferedReader reader,
                  BufferedWriter writer,
                  ESTATE nextState)
    {
        this.nextState = nextState;
        this.socket = socket;
        this.writer = writer;
        this.reader = reader;
        this.hasNext = true;
    }

    public abstract void run();

    public boolean hasNext()
    { return hasNext; }

    public ESTATE getNextState()
    { return nextState; }


    private void setTimeOut(int ms) {
        try {
            socket.setSoTimeout(ms);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    protected boolean write(String msg)
    {
        try {
            System.out.println("SENDING : " + msg);
            writer.write(msg + (char) 7 + "\b");
            writer.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    private int readChar(){
        int c = -1;
        try {
            c = reader.read();
            while (c == -1) {
                c = reader.read();
            }
        }
        catch (SocketTimeoutException e)
        {
            System.out.println("SOCKET TIMEOUT");
            return -1;
        }catch (IOException e)
        {
            e.printStackTrace();
        }
        return c;
    }

    protected boolean read(int maxLen) {
        StringBuilder res = new StringBuilder();
        int len = 0;

        boolean recharging = false;
        while(true)
        {
            if (len >= maxLen && len > Messages.CLIENT_FULL_POWER_LEN )
            {
                write(Messages.SERVER_SYNTAX_ERROR);
                return false;
            }

            int c = readChar();
            if (c == -1)
                return false;


            if(c == 7)
            {
                int l = readChar();
                if (l == -1)
                    return false;

                if (l == 8) {
                    if (len == Messages.CLIENT_FULL_POWER_LEN){
                        if (res.toString().equals(Messages.CLIENT_RECHARGING)){
                            len = 0;
                            res.setLength(0);
                            if (recharging) {
                                write(Messages.SERVER_LOGIC_ERROR);
                                return false;
                            }
                            setTimeOut(5*1000);
                            recharging = true;}
                        else if(res.toString().equals(Messages.CLIENT_FULL_POWER)){
                            len = 0;
                            res.setLength(0);
                            if (!recharging){
                                write(Messages.SERVER_LOGIC_ERROR);
                                return false;
                            }
                            setTimeOut(1000);
                            recharging = false;
                        }
                    }
                    else if(recharging) {
                        write(Messages.SERVER_LOGIC_ERROR);
                        return false;

                    }
                    if (!recharging && len > 0) {
                        break;
                    }
                }
                else {
                    len+=2;
                    res.append((char)c);
                    res.append((char)l);
                }
            }
            else
            {
                res.append((char) c);
                ++len;
            }
        }

        this.currentMsg = res.toString();
        return true;
    }

}
