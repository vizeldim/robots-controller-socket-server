package conv;

public class Converter {
    public static int toHash(String str)
    { return (sumASCII(str) * 1000) % 65536; }

    private static int sumASCII(String str)
    { return str.chars().reduce(0, Integer::sum); }

    public static int code(int hash, int serverKey)
    { return (hash + serverKey) % 65536; }

    public static int decode(int codeFromClient, int clientKey)
    {
        int res = (codeFromClient - clientKey) % 65536;
        return res > 0 ? res : res + 65536;
    }
}
