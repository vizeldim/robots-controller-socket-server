package conv;

public enum EDIR {
    LEFT,RIGHT,UP,DOWN,UNKNOWN,REACHED;

    public static EDIR left(EDIR d) {
        EDIR res = d;
        switch (d){
            case LEFT -> res = DOWN;
            case DOWN -> res = RIGHT;
            case RIGHT -> res = UP;
            case UP -> res = LEFT;
        }
        return res;
    }

    public static EDIR right(EDIR d) {
        EDIR res = d;
        switch (d){
            case LEFT -> res = UP;
            case UP -> res = RIGHT;
            case RIGHT -> res = DOWN;
            case DOWN -> res = LEFT;
        }
        return res;
    }
}
